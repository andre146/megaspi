/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   megaSPI.h
 * Author: andre
 *
 * Created on 17. Dezember 2020, 11:30
 */

#ifndef MEGASPI_H
#define MEGASPI_H

#include <avr/io.h>
#include <stdint.h>

#define SPI_DDR_MOSI    DDRB
#define SPI_PIN_MOSI    PB3
#define SPI_DDR_MISO    DDRB
#define SPI_PIN_MISO    PB4
#define SPI_DDR_SCK     DDRB
#define SPI_PIN_SCK     PB5
#define SPI_DDR_SS      DDRB
#define SPI_PIN_SS      PB2

#define SPI_DORD_LSB 1
#define SPI_DORD_MSB 0
#define SPI_CLK_POL0 0
#define SPI_CLK_POL1 1
#define SPI_CLK_PHA0 0
#define SPI_CLK_PHA1 1

#define SPI_PRESC_2 0b100
#define SPI_PRESC_4 0b000
#define SPI_PRESC_8 0b101
#define SPI_PRESC_16 0b001
#define SPI_PRESC_32 0b110
#define SPI_PRESC_64 0b010
#define SPI_PRESC_128 0b011
#define SPI_PRESC_2 0b100

/*setup SPI in Master mode with the requested parameters*/
extern void spi_master_init(uint8_t data_order, uint8_t clk_pol, uint8_t clk_phase, uint8_t prescaler); 

/*change the prescaler after the SPI has been initialized*/
extern void spi_set_prescaler(uint8_t presc);

/*transmit a byte and return the response*/
extern uint8_t spi_transmit_byte(uint8_t data);

/*send an arbitrary amount of bytes and read back the response*/
extern void spi_transmit_array(uint8_t *input_data, uint8_t *response_data, uint32_t length);

/*send an arbitrary amount of bytes and ignore the response*/
extern void spi_send_array(uint8_t *input_data, uint32_t length);


#endif /* MEGASPI_H */

