/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "megaSPI.h"

void spi_master_init(uint8_t data_order, uint8_t clk_pol, uint8_t clk_phase, uint8_t prescaler){ //setup SPI in Master mode with the requested parameters
    SPI_DDR_MOSI |= (1<<SPI_PIN_MOSI); //setup MOSI, SCK and SS as outputs
    SPI_DDR_SCK |= (1<<SPI_PIN_SCK);
    SPI_DDR_SS |= (1<<SPI_PIN_SS);
    SPI_DDR_MISO &= ~(1<<SPI_PIN_MISO); //setup MISO as input
    SPCR = (1<<SPE) | ((data_order & 1) << DORD) | (1<<MSTR) | ((clk_pol & 1) << CPOL) | ((clk_phase & 1) << CPHA) | ((prescaler & 0b011) << SPR0); //set SPI control register
    SPSR |= ((prescaler & 0b100) >> 2); //set double SPI speed to achive the requested prescaler
}

void spi_set_prescaler(uint8_t presc){  //change the prescaler after the SPI has been initialized
    SPCR &= ~(0b011 << SPR0);   // set the prescaler to zero
    SPCR |= ((presc & 0b011) << SPR0);
    SPSR &= ~(0b100 >> 2);
    SPSR |= ((presc & 0b100) >> 2);
}

uint8_t spi_transmit_byte(uint8_t data){ //transmit a byte and return the response
    SPDR = data; //write data to the data register
    while(!(SPSR & (1<<SPIF))){} //wait for the transaction to finish
    return(SPDR); //the slave response is now shifted into the data register
}

void spi_transmit_array(uint8_t *input_data, uint8_t *response_data, uint32_t length){ //send an arbitrary amount of bytes and read back the response
    for(uint32_t i = 0; i<length; i++){
        SPDR = input_data[i]; //write data to data register
        while(!(SPSR & (1<<SPIF))){} //wait for the transaction to finish
        response_data[i] = SPDR; //read back the slave's response
    }
}

void spi_send_array(uint8_t *input_data, uint32_t length){ //send an arbitrary amount of bytes and ignore the response
    for(uint32_t i = 0; i<length; i++){
        SPDR = input_data[i]; //write data to data register
        while(!(SPSR & (1<<SPIF))){} //wait for the transaction to finish
    }
}
