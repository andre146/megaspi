#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define __AVR_ATmega328__

#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <util/delay.h>
//#include "uart/uart.h"
//#include "uart/uart.c"
#include "megaSPI.h"
#include "megaSPI.c"

void delayMs(uint16_t ms){
    for(; ms > 0; ms--){
         _delay_ms(1);
    }
}

int main(void) {
    
    //uart_init(UART_BAUD_SELECT(19200, F_CPU));
    //uart_puts("Hallo Welt!\n");
    
    spi_master_init(SPI_DORD_MSB, SPI_CLK_POL1, SPI_CLK_PHA1, SPI_PRESC_16);    

    //sei();
    
//    uint8_t counter = 0;
    uint8_t data[] = "Hallo Welt!";
    uint8_t response[12];
    char buffer[12];
    
    spi_set_prescaler(SPI_PRESC_8);
    while (1) {
       delayMs(1000);
       spi_transmit_array(data, response, 12);
      // sprintf(buffer, "Response: %s\n", response);
      // uart_puts(buffer);
    }
    
}
